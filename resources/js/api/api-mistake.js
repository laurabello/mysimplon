$(document).ready(function(){
  $('select').formSelect();
});

(document.querySelector("#addMist")).addEventListener("click", function () {
  update()
  event.preventDefault()
})
var tbody = document.querySelector("#tabMist")

var myInit = {
  method: "GET",
  headers: { "Content-Type": "application/json" }
}

//Ajax to update the output when a mistake is added
function update() {
  var who = document.querySelector("#who").value
  if (window.fetch) {
    fetch("/api-mist?nom="+who, myInit)
      .then(response => {
        return response.json()
      })
      .then(text => {
        while (tbody.firstChild) {
          tbody.removeChild(tbody.firstChild)
        }
        text.forEach(element => {
          let pers = Object.values(element)
          let tr = document.createElement("tr")
          tbody.appendChild(tr)
          pers.forEach(elem => {
            let td = document.createElement("td")
            tr.appendChild(td)
            td.innerText = elem
          })
        });
      });
  }
}


/*Essai de fetch pour remplir l'auto complete Mateialize
window.onload = fill

var myInit = {
  method: "GET",
  headers: { "Content-Type": "application/json" }
}

var options = []

function fill() {
if (window.fetch) {
  fetch("/api-mist", myInit)
    .then(response => {
      return response.json()
    })
    .then(text => {
      console.log(text)
      text.forEach(element => {
        let option = element.nom + element.prenom
        options.push(option)
      });
    });
}
  return options
}

document.addEventListener('DOMContentLoaded', function () {
  var elems = document.querySelectorAll('.autocomplete');
  var instances = M.Autocomplete.init(elems, options);
});*/



