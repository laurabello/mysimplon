<div class="container" >
  <div class="row">
    <div class="col l12 s12 center-align">
    <p class="white-text">Rodez Promo 2 - 2019</p>
    <p class="grey-text text-lighten-4"><address>CCI, Avenue de Bruxelles – 12000 RODEZ</address></p>
    </div>
  </div>
</div>
<div class="footer-copyright" style="background: #2B2B2B">
  <div class="container">
  © Simplon.co
  <a class="grey-text text-lighten-4 right" href="mentions-legales">Mentions légales</a>
  </div>
</div>