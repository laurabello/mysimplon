<div class="mini-header nav-mysimplon">
  <div class="container"></div>
</div>
<nav class="nav-mysimplon nav-extended" style="background: black">
  <div class="nav-wrapper container">
    <a href="/" class="brand-logo left"><img src='/img/logo.png' width="60" height="60" alt="Retour vers l'accueil"></a>
    <a href="#" data-target="mobile-demo" class="sidenav-trigger right"><i class="material-icons">menu</i></a>
    <ul class="right hide-on-med-and-down" style="text-transform: uppercase; font-weight: bold">
      <li><a href="{{ route('trombi') }}"><i class="material-icons right">face</i> Trombinoscope</a></li>
      <li><a href="{{ route('groupes') }}"><i class="material-icons right">group_add</i> Générateur de groupes</a></li>
      <li><a href="{{ route('fautes') }}"><i class="material-icons right">spellcheck</i> Compteur de fautes</a></li>
    </ul>
  </div>
  <ul class="sidenav" id="mobile-demo">
    <li><a href="{{ route('trombi') }}"><i class="material-icons right">face</i> Trombinoscope</a></li>
    <li><a href="{{ route('groupes') }}"><i class="material-icons right">group_add</i> Générateur de groupes</a></li>
    <li><a href="{{ route('fautes') }}"><i class="material-icons right">spellcheck</i> Compteur de fautes</a></li>
  </ul>
</nav>