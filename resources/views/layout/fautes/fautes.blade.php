@extends('layout')

@section('title')
  {{$title}}
  @parent
@endsection

@section('content')
<script src="/js/api-mistake.js" defer></script>
  <main>
    <h1 class="center-align">Compteur de fautes</h1>
    <form class="container">
      <fieldset>
        <legend>Ajouter des fautes</legend>
        <div class="input-field col s12">
          <i class="material-icons prefix">spellcheck</i>
          <select id="who">
            @foreach ($simplonians as $simplonian)
              <option value="{{ $simplonian->firstname }}">{{ $simplonian->firstname }} {{ $simplonian->lastname }}
            @endforeach  
          </select>
          <label for="who">Choisir quelqu'un</label>
        </div>
        <!-- Modal Trigger -->
        <a class="waves-effect waves-light btn modal-trigger" href="#modal1">Ajouter une faute</a>

        <!--<button id="addMist" type=submit>Ajouter une faute</button>-->
      </fieldset>
    </form>
    <table class="container">
      <thead>
        <tr>
          <th>Prénom</th>
          <th>Nom</th>
          <th>Nombre de fautes</th>
          <th>Gâteaux faits</th>
          <th>Gâteaux à faire</th>
        </tr>
      </thead>
      <tbody id="tabMist">
        @foreach ($simplonians as $pers)
          <tr>
              <td> {{ $pers->firstname }} </td> 
              <td> {{ $pers->lastname }} </td>
              <td> {{ $pers->mistakes }} </td>
              <td> {{ $pers->cake_number }} </td>
              <td> {{ $pers->cake_pending }} </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    <!-- Modal Structure -->
    <div id="modal1" class="modal">
      <div class="modal-content">
        <h4>Modal Header</h4>
        <p>Êtes vous sûr?</p>
      </div>
      <div class="modal-footer">
        <a href="#!" id="addMist" class="modal-close waves-effect waves-green btn-flat">Oui!</a>
        <button id="addMist" class="modal-close waves-effect waves-green btn-flat">Nope</button>
      </div>
    </div>
  </main>
  <script>
    $(document).ready(function(){
      $('.modal').modal();
    })
  </script>
@endsection
