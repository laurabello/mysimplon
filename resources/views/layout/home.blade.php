@extends('layout')

@section('content')


<!-- Home page photo -->
<div class="home-photo">
  <div class="card-panel">
    Bienvenue à la Promo 2 de Rodez !
  </div>
</div>
<!-- Trombinoscope preview -->
<section>
  <div class="container home-trombi">
    <p>Trombinoscope</p>
    <div class="home-trombi-group">
      <div class="home-trombi-icon">
        <img class="circle" src="img/graduate-student-avatar.svg" alt="Students">
        <p>{{$nbAppr}} Apprenants</p>
      </div>
      <div class="home-trombi-icon">
        <img class="circle" src="img/teacher-pointing-blackboard.svg" alt="Teachers">
        <p>{{$nbTeach}} Formateurs</p>
      </div>
    </div>
    <a class="btn btn-mysimplon" href="trombi">Accéder au trombinoscope</a>
  </div>
</section>
<!-- Group generator preview -->
<section>
  <div class="container row home-group-machine">
    <p class="col s12">Machine à groupes</p>
    <div class="home-group-list">
    </div>
    <a class="btn btn-mysimplon" href="group">Accéder à la machine</a>
  </div>
</section>

@endsection