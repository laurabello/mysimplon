@extends('layout')

@section('title')
  {{$title}}
  @parent
@endsection

@section('content')
<script src="./src/js/global/trombi_edition.js" defer></script>

  <main>
  <!-- TITLE -->
    <h1 class="trombi-edit center-align">Trombinoscope</h1>
    <h2 class="trombi-edit_sub center-align">Ajouter ou modifier un profil</h2>
  <!-- FORM TO EDIT/ADD PROFILE -->
    <form class="container row">
    <!-- Last name -->
      <div class="input-field col s12 m6">
        <i class="material-icons prefix">account_circle</i>
        <input id="edit-name" class="validate" type="text">
        <label for="edit-name">Nom : </label>
      </div>
    <!-- First name -->
      <div class="input-field col s12 m6">
        <i class="material-icons prefix">account_circle</i>
        <input id="edit-first_name" class="validate" type="text">
        <label for="edit-first_name">Prénom : </label>
      </div>
    <!-- Email -->
      <div class="input-field col s12 m6">
        <i class="material-icons prefix">alternate_email</i>
        <input id="edit-email" class="validate" type="email">
        <label for="edit-email">E-mail : </label>
      </div>
    <!-- Discord -->
      <div class="input-field col s12 m6">
        <i class="fab fa-discord prefix"></i>
        <input id="edit-discord" class="validate" type="text">
        <label for="edit-discord">Discord : </label>
      </div>
    <!-- Github -->
      <div class="input-field col s12 m6">
        <i class="fab fa-github prefix"></i>
        <input id="edit-github" class="validate" type="text">
        <label for="edit-github">Github : </label>
      </div>
    <!-- Gitlab -->
      <div class="input-field col s12 m6">
        <i class="fab fa-gitlab prefix"></i>
        <input id="edit-gitlab" class="validate" type="text">
        <label for="edit-gitlab">Gitlab : </label>
      </div>
    <!-- Website -->
      <div class="input-field col s12 m6">
        <i class="material-icons prefix">language</i>
        <input id="edit-website" class="validate" type="url">
        <label for="edit-website">Site web : </label>
      </div>
    <!-- Harry Potter -->
      <div class="input-field col s12 m6">
        <i class="material-icons prefix">offline_bolt</i>
        <input id="edit-potter" class="validate" type="text">
        <label for="edit-potter">Personnage Harry Potter : </label>
      </div>
    <!-- Status -->
      <div class="input-field col s12 m6">
        <i class="material-icons prefix">transfer_within_a_station</i>
        <select id="edit-status">
          <option disabled selected></option>
          <option>En stage</option>
          <option>En formation</option>
          <option>Absent</option>
        </select>
        <label for="edit-status">Statut</label>
      </div>
    <!-- Type -->
      <div class="input-field col s12 m6">
        <i class="material-icons prefix">face</i>
        <select id="edit-type">
          <option disabled selected></option>
          <option>Apprenant</option>
          <option>Équipe pédagogique</option>
        </select>
        <label for="edit-type">Type</label>
      </div>
    <!-- Checkbox to validate changes -->
      <label class="validate-changes col s12" for="validate-changes">
        <input type="checkbox" id="validate-changes">
        <span>Je suis sûr de vouloir enregistrer les changements</span>
      </label>
    <!-- Submit button -->
      <div class="center-align">
        <button class="btn waves-effect waves-light trombi-save-btn" type="submit" name="action">Enregistrer<i class="material-icons right">send</i></button>
      </div>
    </form>
  </main>
@endsection