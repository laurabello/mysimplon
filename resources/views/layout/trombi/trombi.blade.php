@extends('layout')

@section('title')
  {{$title}}
  @parent
@endsection

@section('content')
<?//php       dd($simplonians); ?>
<div class="container">
  <h2 class="trombi-title"><i class="fas fa-chalkboard-teacher circle-icon"></i> Equipe pédagogique</h2>
  <div class="row">
     @foreach ($simplonians->where('type_id', 2) as $pers)
      <div class="col s12 m6 l4">
        <div class="card">
          <div class="card-image">
            @if ($pers->avatar == 0)
            <img src="img/default_avatar.jpg">
            @else
            <img src="img/assets/trombi/{{$pers->id}}.jpg">
            @endif
            <a class="btn-floating halfway-fab waves-effect waves-light red modal-trigger" href="#modal{{$pers->id}}"><i class="material-icons">zoom_in</i></a>
          </div>
          <div class="card-content">
            <span class="card-title">{{$pers->firstname}} {{$pers->lastname}}</span>
            <p>Equipier pédagogique</p>
          </div>
        </div>
      </div>
    @endforeach
  </div>
  <h2 class="trombi-title"><i class="fas fa-user-graduate circle-icon"></i> Apprenants</h2>
  <div class="row">
    @foreach ($simplonians->where('type_id', 1) as $pers)
      <div class="col s12 m6 l4">
        <div class="card">
          <div class="card-image">
            @if ($pers->avatar == 0)
            <img src="img/default_avatar.jpg">
            @else
            <img src="img/assets/trombi/{{$pers->id}}.jpg">
            @endif
            <a class="btn-floating halfway-fab waves-effect waves-light red modal-trigger" href="#modal{{$pers->id}}"><i class="material-icons">zoom_in</i></a>
          </div>
          <div class="card-content">
            <span class="card-title">{{$pers->firstname }} {{$pers->lastname}}</span>
            <p>Apprenant - {{$pers->status->status_name}}</p>
          </div>
        </div>
      </div>
    @endforeach
  </div>
</div>

@foreach ($simplonians as $pers)
  <div id="modal{{$pers->id}}" class="modal bottom-sheet">
    <div class="modal-content">
      <h4>{{$pers->firstname}} {{$pers->lastname}}</h4>
      <p>{{$pers->type_name}} - {{$pers->status_name}}</p>
      <ul class="collection">
        <li class="collection-item avatar">
          <i class="fas fa-at circle red"></i>
          <span class="title">Adresse mail</span>
          <p><a href="mailto:{{$pers->email}}">{{$pers->email}}</a></p>
        </li>
        <li class="collection-item avatar">
          <i class="fab fa-discord circle red"></i>
          <span class="title">Identifiant Discord</span>
          <p>{{$pers->discord}}</p>
        </li>
        <li class="collection-item avatar">
          <i class="fab fa-github circle red"></i>
          <span class="title">Identifiant Github</span>
          <p>{{$pers->github}}</p>
        </li>
        <li class="collection-item avatar">
          <i class="fab fa-gitlab circle red"></i>
          <span class="title">Identifiant Gitlab</span>
          <p>{{$pers->gitlab}}</p>
        </li>
        @if ($pers->website != '')
          <li class="collection-item avatar">
            <i class="fas fa-globe circle red"></i>
            <span class="title">Site web</span>
            <p><a href="https://{{$pers->website}}">{{$pers->website}}</a></p>
          </li>
        @endif
        @if ($pers->potter != '')
          <li class="collection-item avatar">
            <i class="fas fa-quidditch circle red"></i>
            <span class="title">Personnage de Harry Potter</span>
            <p>{{$pers->potter}}</p>
          </li>
        @endif
        @if ($pers->gateaux > 0)
          <li class="collection-item avatar">
            <i class="fas fa-birthday-cake circle red"></i>
            <span class="title">Gateaux préparés</span>
            <p>{{$pers->cake_number}}</p>
          </li>
        @endif 
        @if ($pers->fautes)
          <li class="collection-item avatar">
            <i class="material-icons circle red">spellcheck</i>
            <span class="title">Nombre de fautes</span>
            <p>{{$pers->mistakes}}</p>
          </li>
        @endif 
      </ul>
    </div>
    <div class="modal-footer">
      <div class="fixed-action-btn">
        <a class="btn-floating btn-large red">
          <i class="large material-icons">mode_edit</i>
        </a>
      </div>
    </div>
    <div class="modal-footer">      
      <div class="fixed-action-btn">
        <a class="btn-floating btn-large red">
            <i class="large material-icons">settings</i>
        </a>
        <ul>
          <li>
            <a class="btn-floating red" href="trombinoscope/edition/{{$pers->id}}"><i class="material-icons">mode_edit</i></a>
          </li>
          <li>
            <a class="btn-floating red darken-1" href="trombinoscope/delete/{{$pers->id}}"><i class="material-icons">delete</i></a>
          </li>
        </ul>
      </div>           
    </div>
  </div>
@endforeach
<div class="fixed-action-btn">
  <a href="{{ route('trombi_edit') }}" class="btn-floating btn-large red">
    <i class="large material-icons">add</i>
  </a>
</div>

<script>
  $(document).ready(function(){
      $('.modal').modal();
  })

  $(document).ready(function(){
      $('.fixed-action-btn').floatingActionButton();
  });
</script>
@endsection