@extends('layout')

@section('title')
  {{$title}}
  @parent
@endsection

@section('content')
<script src="./src/js/global/groups.js" defer></script>
<script src="./src/js/api/api-groups.js" defer></script>

<script src="./src/js/global/groups.js" defer></script>
<script src="./src/js/api/api-groups.js" defer></script>

  <main>
    <!-- TABS -->
    <div class="row">
      <ul class="tabs">
        <li class="tab col s6"><a class="active" href="#create">Créer des groupes</a></li>
        <li class="tab col s6"><a href="#history">Historique des groupes</a></li>
      </ul>
    </div>
    <!-- CREATE GROUPS -->
    <div class="container" id="create">
      <form class="groups-form">
      <!-- Input group number -->
        <div class="groups-input">
          <div class="input-field">
            <i class="material-icons prefix">group_add</i>
            <input id="group-number" type="number" class="validate">
            <label class="active" for="group-number">Combien de groupes voulez-vous?</label>
          </div>
          <!--Checkox pedagogic team -->
          <label class="pedagogic" for="pedagogic">
              <input type="checkbox" id="pedagogic">
              <span>Inclure l'équipe pédagogique</span>
          </label>
        </div>
        <button id="generate" class="btn waves-effect waves-light" type="submit" name="action">
          Générer les groupes
          <i class="material-icons right">send</i>
        </button>
      </form>
      <!-- Created groups result -->
      <div class="container section">
        <ul id="groupir" class="collection with-header">
          <li class="collection-header">Groupes créés</li>
        </ul>
      </div>
      <!-- Save groups -->
      <div class="container save-groups">
        <p>Enregistrer les groupes :</p>
        <div class="input-field">
          <i class="material-icons prefix">group</i>
          <input id="save-group" type="text" class="validate">
          <label class="active" for="save-group">Choisir un nom</label>
        </div>
        <button class="btn waves-effect waves-light" type="submit" name="action">
          Enregistrer
          <i class="material-icons right">save_alt</i>
        </button>
      </div>
    </div>
<!-- GROUP HISTORY -->
  <div class="container" id="history">
    <ul class="collapsible">
      <li>
        <div class="collapsible-header">
          <i class="material-icons">group</i>Groupe 2 - 22/03/19
        </div>
        <div class="collapsible-body history-list">
          <ul class="collection with-header">
            <li class="collection-header history-list-title"><span>3</span> groupes</li>
            <li class="collection-item avatar group-col">
              <img class="circle" src="src/icons/bat.svg" alt="Group image">
              <span class="title">Groupe 1</span>
              <p><span>Germain</span>, <span>Ignace</span>, <span>Mireille</span></p>
            </li>
            <li class="collection-item avatar group-col">
              <img class="circle" src="src/icons/bat.svg" alt="Group image">
              <span class="title">Groupe 1</span>
              <p><span>Germain</span>, <span>Ignace</span>, <span>Mireille</span></p>
            </li>
            <li class="collection-item avatar group-col">
              <img class="circle" src="src/icons/bat.svg" alt="Group image">
              <span class="title">Groupe 1</span>
              <p><span>Germain</span>, <span>Ignace</span>, <span>Mireille</span></p>
            </li>
          </ul>
        </div>
      </li>
      <li>
        <div class="collapsible-header">
          <i class="material-icons">group</i>Groupe 1 - 10/03/19
        </div>
        <div class="collapsible-body history-list">
          <ul class="collection with-header">
            <li class="collection-header history-list-title"><span>3</span> groupes</li>
            <li class="collection-item avatar group-col">
              <img class="circle" src="src/icons/bat.svg" alt="Group image">
              <span class="title">Groupe 1</span>
              <p><span>Germain</span>, <span>Ignace</span>, <span>Mireille</span></p>
            </li>
            <li class="collection-item avatar group-col">
              <img class="circle" src="src/icons/bat.svg" alt="Group image">
              <span class="title">Groupe 1</span>
              <p><span>Germain</span>, <span>Ignace</span>, <span>Mireille</span></p>
            </li>
            <li class="collection-item avatar group-col">
              <img class="circle" src="src/icons/bat.svg" alt="Group image">
              <span class="title">Groupe 1</span>
              <p><span>Germain</span>, <span>Ignace</span>, <span>Mireille</span></p>
            </li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
  </main>
@endsection