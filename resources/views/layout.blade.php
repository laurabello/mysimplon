<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" type="text/css" media="screen" href="/css/app.css" />
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!-- Compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.0/css/all.css" integrity="sha384-Mmxa0mLqhmOeaE8vgOSbKacftZcsNYDjQzuCOm6D02luYSzBG8vpaOykv9lFQ51Y" crossorigin="anonymous">
  <!-- Compiled and minified JavaScript -->
  <script src="http://code.jquery.com/jquery-3.3.1.min.js"></script>
  <title>
    @section('title')
    My Simplon
    @show
  </title>
</head>
<body>
  <header>
    @include('layout.partial.header')
  </header>
  <main>
    <?php /*if (isset($title)): ?>
      <div class="module-illus">
        <div class="module-illus-layer">
            <div class="valign-wrapper container" style="height: 100%">
                <h1 class="center-align module-title" style="width: 100%; font-size: 2.4rem"><?=$title?></h1>
            </div>
        </div>
      </div>
    <?php endif; */?>
    @yield('content')
  </main>
  <footer class="page-footer" style="background: black">
    @include('layout.partial.footer')
  </footer>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
</body>
</html>