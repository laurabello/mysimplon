<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSimploniansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('simplonians', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('potter');
            $table->string('email');
            $table->string('discord');
            $table->string('gitlab');
            $table->string('github');
            $table->string('website');
            $table->integer('mistakes');
            $table->integer('cake_number');
            $table->boolean('cake_pending');
            $table->integer('status_id')->references('id')->on('statuses');
            $table->integer('type_id')->references('id')->on('type');
            $table->boolean('avatar')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('simplonians');
    }
}
