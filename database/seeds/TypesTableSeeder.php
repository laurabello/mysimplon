<?php

use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $types = ['Apprenant', 'Equipier pédagogique'];
      for ($i = 0; $i<count($types); $i++){
        DB::table('types')->insert([
          'type_name' => $types[$i]
        ]);
      }

    }
}
