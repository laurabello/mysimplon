<?php

use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $status = ['Présent', 'En stage', 'Absent'];
      for ($i = 0; $i<count($status); $i++){
        DB::table('statuses')->insert([
          'status_name' => $status[$i]
        ]);
      }

    }
}
