<?php

use Illuminate\Database\Seeder;

class SimploniansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       if(($handle = fopen("./database/seeds/seeds.csv", "r")) !== false)  {
         while(($data = fgetcsv($handle, 1000, "," )) !== false){
            DB::table('simplonians')->insert([
              'firstname' => $data[0],
              'lastname' => $data[1],
              'potter' => $data[2],
              'email' => $data[3],
              'discord' => $data[4],
              'gitlab' => $data[5],
              'github' => $data[6],
              'website' => $data[7],
              'mistakes' => $data[8],
              'cake_number' => $data[9],
              'cake_pending' => $data[10],
              'status_id' => $data[11],
              'type_id' => $data[12],
              'avatar' => $data[13]
            ]);
         }
       }
    }
}