<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Simplonian extends Model
{
    public function type(){
      return $this->belongsTo('App\Type');
    }

    public function status(){
      return $this->belongsTo('App\Statuses');
    }
}
