<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LegalsController extends Controller
{
    public function legals(){
      $title = 'Mentions légales | ';
      return view('layout/mentions_legales', ['title' => $title]);
    }
}
