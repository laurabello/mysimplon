<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TrombiEditController extends Controller
{
    public function trombiEdit(){
      $title = 'Editer le trombi | ';
      return view('layout/trombi/trombi_edition', ['title' => $title]);
    }
}
