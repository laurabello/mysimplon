<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home() {
      $appr = \App\Simplonian::where('type_id', 1)->count();
      $teach = \App\Simplonian::where('type_id', 2)->count();
      return view('layout/home', ['nbAppr' => $appr, 'nbTeach' => $teach]);
    }
}