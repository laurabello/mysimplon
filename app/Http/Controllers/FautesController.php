<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FautesController extends Controller
{
    public function fautes(){
      $simplonians = \App\Simplonian::select('firstname', 'lastname', 'mistakes', 'cake_number', 'cake_pending')->get();

      $title = "Compteur de fautes | ";
      return view('layout/fautes/fautes', ['simplonians' => $simplonians, 'title' => $title]);
    }
}
