<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TrombiController extends Controller
{
    public function trombi() {
      $simplonians = \App\Simplonian::select('*')->get();
      $title = 'Trombinoscope | ';
      return view('layout/trombi/trombi', ['title' => $title, 'simplonians' => $simplonians]);
    }
}