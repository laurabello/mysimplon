<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GroupsController extends Controller
{
    public function groups(){
      $title = 'Générateur de groupes | ';
      return view('layout/group/groups', ['title' => $title]);
    }
}
