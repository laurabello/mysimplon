<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@home']);

ROUTE::get('/fautes', ['as' => 'fautes', 'uses' => 'FautesController@fautes']);

ROUTE::get('/groupes', ['as' => 'groupes', 'uses' => 'GroupsController@groups']);

ROUTE::group(['prefix' => 'trombinoscope'], function(){
  ROUTE::get('/', ['as' => 'trombi', 'uses' => 'TrombiController@trombi']);
  ROUTE::get('/edition', ['as' => 'trombi_edit', 'uses' => 'TrombiEditController@trombiEdit']);
});

ROUTE::get('mentions_legales', ['as' => 'legals', 'uses' => 'LegalsController@legals']);